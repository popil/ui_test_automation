#### Prerequisites:
  - Python v3.6+
  - pip package manager
  
#### Getting started:
  - clone the project
  - in the terminal open ```ui_test_automation``` project root directory
  - run ```pip install -r requirements.txt```
  - run tests ```py.test tests --chromedriver <chromedriver_path>``` or ```python -m pytest tests --chromedriver <chromedriver_path>``` (depending on your configuration)
  - once tests are executed the ```report.html``` file will be generated in the project root directory