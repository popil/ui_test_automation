import os
from configparser import ConfigParser


class BaseConfig(object):
    path = os.path.dirname(os.path.abspath(__file__))
    parser = ConfigParser()
    parser.read(path + '/settings.ini')

    def read(self, key):
        section = self.__class__.__name__
        return self.parser.get(section, key)


class Site(BaseConfig):
    def __init__(self):
        self.base_url = self.read('base_url')


class User(BaseConfig):
    def __init__(self):
        self.email = self.read('email')
        self.pwd = self.read('pwd')
