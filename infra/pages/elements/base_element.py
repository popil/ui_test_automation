from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from infra.pages import webdriver_wrapper


class BaseElement(object):

    @property
    def name(self):
        return self.__class__.__name__

    def go_to(self):
        return self

    def wait_for_clickable_element(self, time_sec=5):
        try:
            return WebDriverWait(webdriver_wrapper.driver, time_sec).until(
                EC.element_to_be_clickable((self.locator.by, self.locator.value)))
        except TimeoutException:
            raise TimeoutException("'{}' with {} '{}' is not clickable!".format(
                self.name, self.locator.by, self.locator.value))

    def wait_for_element(self, time_sec=5):
        try:
            return WebDriverWait(webdriver_wrapper.driver, time_sec).until(
                EC.presence_of_element_located((self.locator.by, self.locator.value)))
        except TimeoutException:
            raise TimeoutException("'{}' with {} '{}' is not present!".format(
                self.name, self.locator.by, self.locator.value))

    def wait_for_visibility_of_element(self, time_sec=5):
        try:
            return WebDriverWait(webdriver_wrapper.driver, time_sec).until(
                EC.visibility_of_element_located((self.locator.by, self.locator.value)))
        except TimeoutException:
            raise TimeoutException("'{}' with {} '{}' is not visible!".format(
                self.name, self.locator.by, self.locator.value))

    def is_displayed(self, time_sec=5):
        try:
            return self.wait_for_visibility_of_element(time_sec)
        except Exception:
            return False

    def get_attribute(self, attr):
        return self.find_element().get_attribute(attr)

    @property
    def value(self):
        return self.find_element().get_attribute('value')

    def find_element(self):
        try:
            return webdriver_wrapper.driver.find_element(self.locator.by, self.locator.value)
        except NoSuchElementException:
            raise NoSuchElementException("'{}' not found by {} '{}'".format(
                self.name, self.locator.by, self.locator.value))

    def find_elements(self):
        return webdriver_wrapper.driver.find_elements(self.locator.by, self.locator.value)

    def hover_element(self):
        el = self.wait_for_element()
        ActionChains(webdriver_wrapper.driver).move_to_element(el).perform()
