from infra.pages import webdriver_wrapper
from infra.pages.elements.base_element import BaseElement


class Button(BaseElement):

    def click(self):
        self.wait_for_clickable_element()
        webdriver_wrapper.driver.find_element(self.locator.by, self.locator.value).click()
        return self.go_to()

    @property
    def text(self):
        return self.wait_for_clickable_element().text
