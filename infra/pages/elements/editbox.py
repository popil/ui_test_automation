from selenium.webdriver.common.action_chains import ActionChains

from infra.pages import webdriver_wrapper
from infra.pages.elements.base_element import BaseElement


class EditBox(BaseElement):

    def clear(self):
        el = webdriver_wrapper.driver.find_element(self.locator.by, self.locator.value)
        el.clear()

    @property
    def text(self):
        return self.wait_for_clickable_element().text

    @text.setter
    def text(self, value):
        el = self.wait_for_clickable_element()
        el.click()
        ActionChains(webdriver_wrapper.driver).send_keys(value).perform()

    def click(self):
        elm = self.wait_for_clickable_element()
        elm.click()
        return self.go_to()
