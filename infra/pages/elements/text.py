from infra.pages.elements.base_element import BaseElement


class Text(BaseElement):

    @property
    def text(self):
        elm = self.wait_for_visibility_of_element()
        return elm.text
