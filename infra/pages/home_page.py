from infra.pages import webdriver_wrapper
from infra.pages.base_page import BasePage, Locator
from infra.pages.elements.button import Button
from infra.pages.login_page import LoginPage
from infra.pages.hovers_page import HoversPage
from infra.pages.tables_page import TablesPage


class FormAuthenticationButton(Button):
    def __init__(self):
        self.locator = Locator.css_selector('a[href="/login"]')

    def go_to(self):
        return LoginPage(webdriver_wrapper.driver)


class HoversButton(Button):
    def __init__(self):
        self.locator = Locator.css_selector('a[href="/hovers"]')

    def go_to(self):
        return HoversPage(webdriver_wrapper.driver)


class SortableDataTablesButton(Button):
    def __init__(self):
        self.locator = Locator.css_selector('a[href="/tables"]')

    def go_to(self):
        return TablesPage(webdriver_wrapper.driver)


class HomePage(BasePage):
    def __init__(self, driver):
        super(HomePage, self).__init__(driver)
        self.auth_button = FormAuthenticationButton()
        self.hovers_button = HoversButton()
        self.tables_button = SortableDataTablesButton()
