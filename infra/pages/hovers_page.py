from infra.pages.base_page import BasePage, Locator
from infra.pages.elements.button import Button


class UserImage(Button):
    def __init__(self, number=None):
        self.locator = Locator.css_selector('#content .figure' + (':nth-of-type({})'.format(number) if number else ''))


class HoversPage(BasePage):
    def __init__(self, driver):
        super(HoversPage, self).__init__(driver)
        self.user_image = UserImage
