from infra.pages.base_page import BasePage, Locator
from infra.pages.elements.button import Button
from infra.pages.elements.editbox import EditBox
from infra.pages.elements.text import Text


class FlashMessage(Text):
    def __init__(self):
        self.locator = Locator.id('flash-messages')


class EmailEditBox(EditBox):
    def __init__(self):
        self.locator = Locator.id('username')


class PasswordEditBox(EditBox):
    def __init__(self):
        self.locator = Locator.id('password')


class LoginButton(Button):
    def __init__(self):
        self.locator = Locator.xpath_selector('//button[contains(.,"Login")]')


class LoginPage(BasePage):
    def __init__(self, driver):
        super(LoginPage, self).__init__(driver)
        self.flash_message = FlashMessage()
        self.email_editbox = EmailEditBox()
        self.password_editbox = PasswordEditBox()
        self.login_button = LoginButton()
