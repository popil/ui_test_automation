from infra.pages.base_page import BasePage, Locator
from infra.pages.elements.editbox import EditBox
from infra.pages.elements.button import Button


class LastNameButton(Button):
    def __init__(self):
        self.locator = Locator.css_selector('#table2 th .last-name')


class LastNameItem(EditBox):
    def __init__(self):
        self.locator = Locator.css_selector('#table2 td.last-name')


class TablesPage(BasePage):
    def __init__(self, driver):
        super(TablesPage, self).__init__(driver)
        self.last_name_button = LastNameButton()
        self.last_name_item = LastNameItem()
