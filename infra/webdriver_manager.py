import sys

import pytest
from selenium import webdriver


class Singleton(object):
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = object.__new__(cls)
            return cls._instance


class WebdriverWrapper(Singleton):
    def __init__(self):
        self.driver = None

    def get_webdriver(self):
        chromedriver_path = pytest.config.getoption('chromedriver')
        options = webdriver.ChromeOptions()
        max_window_args = {
            'win32': '--start-maximized',
            'linux2': '--window-size=1920,1080',
            'darwin': '--start-fullscreen'
        }
        options.add_argument(max_window_args[sys.platform])
        options.add_argument('--disable-infobars')
        self.driver = webdriver.Chrome(
            executable_path=chromedriver_path,
            options=options,
        )
        return self.driver
