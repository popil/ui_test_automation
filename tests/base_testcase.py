from config import Site
from infra.pages import webdriver_wrapper


class BaseTestCase(object):
    driver = None

    def setup_method(self):
        url = Site().base_url
        self.driver = webdriver_wrapper.get_webdriver()
        self.driver.get(url)

    def teardown_method(self):
        self.driver.quit()
