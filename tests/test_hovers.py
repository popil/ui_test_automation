from infra.pages.home_page import HomePage
from tests.base_testcase import BaseTestCase


class TestHovers(BaseTestCase):

    def test_username_appears_on_hovering(self):
        home_page = HomePage(self.driver)
        hovers_page = home_page.hovers_button.click()
        for n in range(len(hovers_page.user_image().find_elements())):
            img = hovers_page.user_image(n+1)
            img.hover_element()
            assert 'name: user{}'.format(n+1) in img.text, 'Missing username on {} image'.format(n+1)
