import pytest

from config import User
from infra.pages.home_page import HomePage
from tests.base_testcase import BaseTestCase


class TestLogin(BaseTestCase):
    user = User()

    @pytest.mark.parametrize('email, pwd, message', [
        (user.email, user.pwd, 'You logged into a secure area!'),
        ('incorrect@email.test', user.pwd, 'Your username is invalid!'),
        (user.email, 'IncorrectPassword', 'Your password is invalid!'),
    ])
    def test_login_with_creds(self, email, pwd, message):
        home_page = HomePage(self.driver)
        login_page = home_page.auth_button.click()
        login_page.email_editbox.text = email
        login_page.password_editbox.text = pwd
        login_page.login_button.click()
        assert message in login_page.flash_message.text
