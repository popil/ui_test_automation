from infra.pages.home_page import HomePage
from tests.base_testcase import BaseTestCase


class TestTable(BaseTestCase):

    def test_column_sorting(self):
        home_page = HomePage(self.driver)
        tables_page = home_page.tables_button.click()

        def get_last_names():
            return [elm.text for elm in tables_page.last_name_item.find_elements()]

        tables_page.last_name_button.click()
        asc_order = get_last_names()
        assert asc_order == sorted(asc_order)

        tables_page.last_name_button.click()
        desc_order = get_last_names()
        assert desc_order == sorted(desc_order, reverse=True)
